package Factorias;

import DAO.CRUD;
import DAO.DelegacionDAOSQLImpl;
import DAO.SedeCentralDAOSQLImpl;
import DAO.SocioDAOSQLImpl;
import Enums.TipoEntidades;

public abstract class DAOFactoria {

    public static CRUD getFactoria(TipoEntidades entidad) {
        switch (entidad) {
            case Socio:
                return new SocioDAOSQLImpl();
            case Delegacion:
                return new DelegacionDAOSQLImpl();
            case SedeCentral:
                return new SedeCentralDAOSQLImpl();
            default:
                return null;
        }
    }

}
