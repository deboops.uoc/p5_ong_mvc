package Model;

import DAO.CRUD;
import Enums.TipoEntidades;
import Factorias.DAOFactoria;

import javax.persistence.*;
import java.sql.SQLException;

@Entity
@Table(name="SedeCentral")

public class SedeCentral {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="codigo")
    private int codigo;
    @Column(name="nombre")
    private String nombre;
    @Column(name="cif")
    private String cif;

    public SedeCentral() {
        super();
    }

    public SedeCentral(String nombre, String cif) {
        super();
        this.nombre = nombre;
        this.cif = cif;
    }

    public SedeCentral(int idCodigo, String nombre, String cif) {
        this.codigo = idCodigo;
        this.nombre = nombre;
        this.cif = cif;
    }

    public int getCodigo() {
        return codigo;
    }

    private void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    @Override
    public String toString() {
        return "SedeCentral : nombre=" + nombre + ", cif=" + cif ;
    }

    public void actualizarSede() throws SQLException {
        CRUD<SedeCentral> sedeDAO = DAOFactoria.getFactoria(TipoEntidades.SedeCentral);
        sedeDAO.registrar(this);
    }
}
