package Model;

import javax.persistence.*;

@Entity
@Table(name="Delegacion")
public class Delegacion implements Comparable<Delegacion> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="codigo")
    private int codigo;
    @Column(name="nombre")
    private String nombre; // El nombre de la delegación es clave única
    @Column(name="direccion")
    private String direccion;

    public Delegacion() {
    }

    public Delegacion(String nombre, String direccion) {
        this.nombre = nombre;
        this.direccion = direccion;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null) {
            Delegacion delegacionAComparar = (Delegacion) obj;
            return nombre.equals(delegacionAComparar.nombre);
        }
        return false;

    }

    @Override
    public int compareTo(Delegacion delegacionAComparar) {
        return this.nombre.compareTo(delegacionAComparar.nombre);
    }

    @Override
    public String toString() {
        return "Delegacion : nombre: " + nombre + ", direccion: "	+ direccion;
    }
}
