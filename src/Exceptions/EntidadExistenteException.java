package Exceptions;

public class EntidadExistenteException extends RuntimeException {

    public EntidadExistenteException(String mensaje, Throwable err) {
        super(mensaje, err);
    }

    public EntidadExistenteException(String mensaje) {
        super(mensaje);
    }
}
