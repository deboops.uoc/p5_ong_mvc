package Exceptions;

public class NoExisteEntidadException extends RuntimeException {

    public NoExisteEntidadException(String mensaje, Throwable err) {
        super(mensaje, err);
    }

    public NoExisteEntidadException(String mensaje) {
        super(mensaje);
    }
}
