package Controller;

import DAO.CRUD;
import Enums.TipoEntidades;
import Factorias.DAOFactoria;
import Model.Socio;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.hibernate.HibernateException;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GestionarSociosController implements Initializable {
    public TableView<Socio> tablaSocios;
    public TableColumn<Socio, Integer> colSocioCodigo;
    public TableColumn<Socio, String> colSocioNIF;
    public TableColumn<Socio, String> colSocioNombre;
    public TableColumn<Socio, String> colSocioFechaAlta;
    public TableColumn<Socio, String> colSocioFechaBaja;
    public Button btnCrearSocio;
    public Button btnModificarSocio;
    public Button btnEliminarSocio;
    public Button btnBajaSocio;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.colSocioCodigo.setCellValueFactory(new PropertyValueFactory("codigo"));
        this.colSocioNIF.setCellValueFactory(new PropertyValueFactory("identificador"));
        this.colSocioNombre.setCellValueFactory(new PropertyValueFactory("nombre"));
        this.colSocioFechaAlta.setCellValueFactory(new PropertyValueFactory("fechaAlta"));
        this.colSocioFechaBaja.setCellValueFactory(new PropertyValueFactory("fechaBaja"));

        this.cargarTabla();
    }

    private void cargarTabla() {
        List<Socio> socios = new ArrayList<Socio>();

        CRUD<Socio> crudSocio =  DAOFactoria.getFactoria(TipoEntidades.Socio);

        try {
            socios = crudSocio.listarTodos();
            ObservableList<Socio> items = this.getDelegacionesObservableList(socios);
            this.tablaSocios.setItems(items);
        } catch (HibernateException err) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("Ha ocurrido un error al listar los socios.");
            alert.showAndWait();
        }
    }

    private ObservableList<Socio> getDelegacionesObservableList(List<Socio> socios) {
        ObservableList<Socio> obs = FXCollections.observableArrayList();

        socios.stream().forEach((socio)-> {
            obs.add(socio);
        });
        return obs;
    }

    public void closeWindows() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/MenuVista.fxml"));

            Parent root = loader.load();

            Scene scene = new Scene(root);
            Stage stage = new Stage();

            stage.setScene(scene);
            stage.show();

            Stage myStage = (Stage) this.tablaSocios.getScene().getWindow();
            myStage.close();

        } catch (IOException ex) {
            Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onSeleccionarSocio(MouseEvent mouseEvent) {
    }

    public void onCrearSocio(ActionEvent actionEvent) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/GestionarSocioVista.fxml"));
            Parent root = loader.load();

            GestionarSocioController controlador = loader.getController();

            Scene scene = new Scene(root);
            Stage stage = new Stage();

            stage.setScene(scene);
            stage.show();

            stage.setOnCloseRequest(e -> controlador.closeWindows());

            Stage myStage = (Stage) this.tablaSocios.getScene().getWindow();
            myStage.close();

        } catch (IOException ex) {
            Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onModificarSocio(ActionEvent actionEvent) {
        Socio socio = this.tablaSocios.getSelectionModel().getSelectedItem();

        if (socio == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("Debe seleccionar un socio a modificar.");
            alert.showAndWait();
        } else {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/GestionarSocioVista.fxml"));
                Parent root = loader.load();

                GestionarSocioController controlador = loader.getController();
                controlador.initAttributtes(socio);

                Scene scene = new Scene(root);
                Stage stage = new Stage();

                stage.setScene(scene);
                stage.show();

                stage.setOnCloseRequest(e -> controlador.closeWindows());

                Stage myStage = (Stage) this.tablaSocios.getScene().getWindow();
                myStage.close();

            } catch (IOException ex) {
                Logger.getLogger(GestionarDelegacionController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void onEliminarSocio(ActionEvent actionEvent) {
        Socio socio = this.tablaSocios.getSelectionModel().getSelectedItem();

        if (socio == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("Debe seleccionar un socio a eliminar.");
            alert.showAndWait();
        } else {
            CRUD<Socio> crudSocio = DAOFactoria.getFactoria(TipoEntidades.Socio);


                String fechaBaja = socio.getFechaBaja();
                if (fechaBaja == null || fechaBaja == "") {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setHeaderText(null);
                    alert.setTitle("Advertencia");
                    alert.setContentText("Debe de dar de baja el socio antes de eliminarlo.");
                    alert.showAndWait();
                } else {
                    Alert confirmacion = new Alert(Alert.AlertType.CONFIRMATION);
                    confirmacion.setHeaderText(null);
                    confirmacion.setTitle("Confirmación");
                    confirmacion.setContentText("¿está seguro que desea eliminar el socio?");

                    Optional<ButtonType> result = confirmacion.showAndWait();
                    if (result.get() == ButtonType.OK){
                        try {
                            crudSocio.eliminar(socio);
                            this.cargarTabla();
                        } catch (HibernateException err) {
                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            alert.setHeaderText(null);
                            alert.setTitle("Error");
                            alert.setContentText("Ha ocurrido un error al eliminar el socio.");
                            alert.showAndWait();
                        }
                    }
                }
        }
    }

    public void onDarBajaSocio(ActionEvent actionEvent) {
        Socio socio = this.tablaSocios.getSelectionModel().getSelectedItem();

        if (socio == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("Debe seleccionar un socio a dar de baja.");
            alert.showAndWait();
        } else {

            String fechaBaja = socio.getFechaBaja();
            if (fechaBaja == null || fechaBaja == "") {
                CRUD<Socio> crudSocio = DAOFactoria.getFactoria(TipoEntidades.Socio);

                socio.asignarFechaLocalAFechaBaja();
                crudSocio.modificar(socio);
                this.tablaSocios.refresh();
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setTitle("Error");
                alert.setContentText("El socio ya esta dado de baja.");
                alert.showAndWait();
            }
        }

    }
}
