package Controller;

import DAO.CRUD;
import Enums.TipoEntidades;
import Factorias.DAOFactoria;
import Model.SedeCentral;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.hibernate.HibernateException;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CrearSedeCentralController {
    public TextField tfieldSedeCentralNombre;
    public TextField tfieldSedeCentralCIF;
    public Button btnGuardarSedeCentral;

    public void closeWindows() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/GestionarSedeCentralVista.fxml"));

            Parent root = loader.load();

            GestionarSedeCentralController controlador = loader.getController();

            Scene scene = new Scene(root);
            Stage stage = new Stage();

            stage.setScene(scene);
            stage.show();

            stage.setOnCloseRequest(e -> controlador.closeWindows());

            Stage myStage = (Stage) this.btnGuardarSedeCentral.getScene().getWindow();
            myStage.close();

        } catch (IOException ex) {
            Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onGuardarSedeCentral(ActionEvent actionEvent) {
        String errores = this.getFormularioErrores();
        if (errores.length() != 0) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText(errores);
            alert.showAndWait();
        } else {
            String nombre = this.tfieldSedeCentralNombre.getText();
            String CIF = this.tfieldSedeCentralCIF.getText();

            SedeCentral sedeCentralACrear = new SedeCentral(nombre, CIF);

            CRUD<SedeCentral> crudSedeCentral = DAOFactoria.getFactoria(TipoEntidades.SedeCentral);

            try {
                crudSedeCentral.registrar(sedeCentralACrear);

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText(null);
                alert.setTitle("Información");
                alert.setContentText("Se ha creado la sede central correctamente.");
                alert.showAndWait();

            } catch(HibernateException err) {

                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setTitle("Error");
                alert.setContentText("Ha ocurrido un error al crear la sede central.");
                alert.showAndWait();
            }
            this.closeWindows();
        }

    }

    private String getFormularioErrores() {
        String errores = "";

        String nombre = this.tfieldSedeCentralNombre.getText();
        String CIF = this.tfieldSedeCentralCIF.getText();

        if (nombre == null || nombre.trim().length() == 0) {
            errores += "- El campo nombre es invalido\n";
        }

        if (CIF == null || CIF.trim().length() == 0) {
            errores += "- El campo CIF es invalido\n";
        }
        return errores;
    }
}
