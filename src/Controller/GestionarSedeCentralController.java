package Controller;

import DAO.CRUD;
import Enums.TipoEntidades;
import Exceptions.NoExisteEntidadException;
import Factorias.DAOFactoria;
import Model.Delegacion;
import Model.SedeCentral;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.hibernate.HibernateException;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GestionarSedeCentralController implements Initializable {
    private SedeCentral sedeCentral = null;

    public TableView<SedeCentral> tablaSedeCentral;

    public TableColumn<SedeCentral, Integer> colSedeCentralCodigo;
    public TableColumn<SedeCentral, String> colSedeCentralNombre;
    public TableColumn<SedeCentral, String> colSedeCentralCIF;

    public Button btnCrearSedeCentral;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.colSedeCentralCodigo.setCellValueFactory(new PropertyValueFactory("codigo"));
        this.colSedeCentralNombre.setCellValueFactory(new PropertyValueFactory("nombre"));
        this.colSedeCentralCIF.setCellValueFactory(new PropertyValueFactory("cif"));

        this.cargarTabla();

    }

    private void cargarTabla() {
        CRUD<SedeCentral> crudSedeCentral =  DAOFactoria.getFactoria(TipoEntidades.SedeCentral);

        try {
            this.sedeCentral = crudSedeCentral.leerPorId("0");
            ObservableList<SedeCentral> items = this.getSedeCentralObservableList(this.sedeCentral);
            this.tablaSedeCentral.setItems(items);
        } catch (HibernateException err) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("Ha ocurrido un error al buscar la sede central.");
            alert.showAndWait();
        } catch (NoExisteEntidadException err) {
            this.sedeCentral = null;
        }
    }

    private ObservableList<SedeCentral> getSedeCentralObservableList(SedeCentral sedeCentral) {
        ObservableList<SedeCentral> obs = FXCollections.observableArrayList();
        obs.add(sedeCentral);
        return obs;
    }

    public void closeWindows() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/MenuVista.fxml"));

            Parent root = loader.load();

            Scene scene = new Scene(root);
            Stage stage = new Stage();

            stage.setScene(scene);
            stage.show();

            Stage myStage = (Stage) this.tablaSedeCentral.getScene().getWindow();
            myStage.close();

        } catch (IOException ex) {
            Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onSeleccionarSedeCentral(MouseEvent mouseEvent) {
    }

    public void onCrearSedeCentral(ActionEvent actionEvent) {

        if (this.sedeCentral != null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("Error - Solo es posible crear una sede central.");
            alert.showAndWait();
        } else {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/CrearSedeCentralVista.fxml"));
                Parent root = loader.load();

                CrearSedeCentralController controlador = loader.getController();

                Scene scene = new Scene(root);
                Stage stage = new Stage();

                stage.setScene(scene);
                stage.show();

                stage.setOnCloseRequest(e -> controlador.closeWindows());

                Stage myStage = (Stage) this.tablaSedeCentral.getScene().getWindow();
                myStage.close();

            } catch (IOException ex) {
                Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
