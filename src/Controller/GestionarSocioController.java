package Controller;

import DAO.CRUD;
import Enums.TipoEntidades;
import Factorias.DAOFactoria;
import Model.Socio;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.hibernate.HibernateException;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GestionarSocioController {
    private Socio socioSeleccionado = null;

    public TextField tfieldSocioNIF;
    public TextField tfieldSocioNombre;
    public Button btnGuardarSocio;

    public void closeWindows() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/GestionarSociosVista.fxml"));

            Parent root = loader.load();

            GestionarSociosController controlador = loader.getController();

            Scene scene = new Scene(root);
            Stage stage = new Stage();

            stage.setScene(scene);
            stage.show();

            stage.setOnCloseRequest(e -> controlador.closeWindows());

            Stage myStage = (Stage) this.btnGuardarSocio.getScene().getWindow();
            myStage.close();

        } catch (IOException ex) {
            Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void initAttributtes(Socio socio) {
        if (socio != null) {
            this.socioSeleccionado = socio;

            this.tfieldSocioNIF.setText(this.socioSeleccionado.getIdentificador());
            this.tfieldSocioNombre.setText(this.socioSeleccionado.getNombre());
        }
    }

    public void onguardarSocio(ActionEvent actionEvent) {
        String errores = this.getFormularioErrores();
        if (errores.length() != 0) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText(errores);
            alert.showAndWait();
        } else {
            if (this.socioSeleccionado == null) {
                this.crearSocio();
            } else {
                this.modificarSocio();
            }
            this.closeWindows();
        }
    }

    private void crearSocio() {
        String NIF = this.tfieldSocioNIF.getText();
        String nombre = this.tfieldSocioNombre.getText();
        Socio socioACrear = new Socio(NIF, nombre);

        CRUD<Socio> crudSocio = DAOFactoria.getFactoria(TipoEntidades.Socio);

        try {
            crudSocio.registrar(socioACrear);

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setTitle("Información");
            alert.setContentText("Se ha creado el socio correctamente.");
            alert.showAndWait();

        } catch(HibernateException err) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("Ha ocurrido un error al crear el socio.");
            alert.showAndWait();
        }

    }

    private void modificarSocio() {
        String NIF = this.tfieldSocioNIF.getText();
        String nombre = this.tfieldSocioNombre.getText();

        this.socioSeleccionado.setIdentificador(NIF);
        this.socioSeleccionado.setNombre(nombre);

        CRUD<Socio> crudSocio = DAOFactoria.getFactoria(TipoEntidades.Socio);

        try {
            crudSocio.modificar(this.socioSeleccionado);

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setTitle("Información");
            alert.setContentText("Se ha modificado el socio correctamente.");
            alert.showAndWait();

        } catch(HibernateException err) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("Ha ocurrido un error al modificar el socio.");
            alert.showAndWait();
        }
        this.socioSeleccionado = null;
    }

    private String getFormularioErrores() {
        String errores = "";

        String NIF = this.tfieldSocioNIF.getText();
        String nombre = this.tfieldSocioNombre.getText();

        if (NIF == null || NIF.trim().length() == 0) {
            errores += "- El campo NIF es invalido\n";
        }

        if (nombre == null || nombre.trim().length() == 0) {
            errores += "- El campo nombre es invalido\n";
        }
        return errores;
    }
}
