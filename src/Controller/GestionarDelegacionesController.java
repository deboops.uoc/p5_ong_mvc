package Controller;

import DAO.CRUD;
import Enums.TipoEntidades;
import Factorias.DAOFactoria;
import Model.Delegacion;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.hibernate.HibernateException;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GestionarDelegacionesController implements Initializable {


    public TableView<Delegacion> tablaDelegaciones;
    public Button btnCrearDelegacion;
    public Button btnModificarDelegacion;
    public Button btnEliminarDelegacion;

    public TableColumn<Delegacion, Integer> colDelegacionCodigo;
    public TableColumn<Delegacion, String> colDelegacionNombre;
    public TableColumn<Delegacion, String> colDelegacionDireccion;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.colDelegacionCodigo.setCellValueFactory(new PropertyValueFactory("codigo"));
        this.colDelegacionNombre.setCellValueFactory(new PropertyValueFactory("nombre"));
        this.colDelegacionDireccion.setCellValueFactory(new PropertyValueFactory("direccion"));

        this.cargarTabla();

    }

    private void cargarTabla() {
        List<Delegacion> delegaciones = new ArrayList<Delegacion>();

        CRUD<Delegacion> crudDelegacion =  DAOFactoria.getFactoria(TipoEntidades.Delegacion);

        try {
            delegaciones = crudDelegacion.listarTodos();
            ObservableList<Delegacion> items = this.getDelegacionesObservableList(delegaciones);
            this.tablaDelegaciones.setItems(items);
        } catch (HibernateException err) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("Ha ocurrido un error al listar las delegaciones.");
            alert.showAndWait();
        }
    }

    private ObservableList<Delegacion> getDelegacionesObservableList(List<Delegacion> delegaciones) {
        ObservableList<Delegacion> obs = FXCollections.observableArrayList();

        delegaciones.stream().forEach((delegacion)-> {
            obs.add(delegacion);
        });
        return obs;
    }

    public void closeWindows() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/MenuVista.fxml"));

            Parent root = loader.load();

            Scene scene = new Scene(root);
            Stage stage = new Stage();

            stage.setScene(scene);
            stage.show();

            Stage myStage = (Stage) this.tablaDelegaciones.getScene().getWindow();
            myStage.close();

        } catch (IOException ex) {
            Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onCrearDelegacion(ActionEvent actionEvent) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/GestionarDelegacionVista.fxml"));
            Parent root = loader.load();

            GestionarDelegacionController controlador = loader.getController();

            Scene scene = new Scene(root);
            Stage stage = new Stage();

            stage.setScene(scene);
            stage.show();

            stage.setOnCloseRequest(e -> controlador.closeWindows());

            Stage myStage = (Stage) this.tablaDelegaciones.getScene().getWindow();
            myStage.close();

        } catch (IOException ex) {
            Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onModificarDelegacion(ActionEvent actionEvent) {
        Delegacion delegacion = this.tablaDelegaciones.getSelectionModel().getSelectedItem();

        if (delegacion == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("Debe seleccionar una delegación a modificar.");
            alert.showAndWait();
        } else {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/GestionarDelegacionVista.fxml"));
                Parent root = loader.load();

                GestionarDelegacionController controlador = loader.getController();
                controlador.initAttributtes(delegacion);

                Scene scene = new Scene(root);
                Stage stage = new Stage();

                stage.setScene(scene);
                stage.show();

                stage.setOnCloseRequest(e -> controlador.closeWindows());

                Stage myStage = (Stage) this.tablaDelegaciones.getScene().getWindow();
                myStage.close();

            } catch (IOException ex) {
                Logger.getLogger(GestionarDelegacionController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void onEliminarDelegacion(ActionEvent actionEvent) {
        Delegacion delegacion = this.tablaDelegaciones.getSelectionModel().getSelectedItem();

        if (delegacion == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("Debe seleccionar una delegación a eliminar.");
            alert.showAndWait();
        } else {
            CRUD<Delegacion> crudDelegacion = DAOFactoria.getFactoria(TipoEntidades.Delegacion);

            Alert confirmacion = new Alert(Alert.AlertType.CONFIRMATION);
            confirmacion.setHeaderText(null);
            confirmacion.setTitle("Confirmación");
            confirmacion.setContentText("¿está seguro que desea eliminar la delegación?");

            Optional<ButtonType> result = confirmacion.showAndWait();
            if (result.get() == ButtonType.OK){
                try {
                    crudDelegacion.eliminar(delegacion);
                    this.cargarTabla();

                } catch (HibernateException err) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setHeaderText(null);
                    alert.setTitle("Error");
                    alert.setContentText("Ha ocurrido un error al eliminar la delegación.");
                    alert.showAndWait();
                }
            }

        }

    }

    public void onSeleccionarDelegacion(MouseEvent mouseEvent) {
    }
}
