package Controller;

import DAO.CRUD;
import Enums.TipoEntidades;
import Exceptions.NoExisteEntidadException;
import Factorias.DAOFactoria;
import Model.SedeCentral;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import org.hibernate.HibernateException;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MenuController {
    public Button btnManageSede;
    public Button btnManageSocios;
    public Button btnManageDelegaciones;

    public void onManageSedeCentral(ActionEvent actionEvent) {
        try {

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/GestionarSedeCentralVista.fxml"));
            Parent root = loader.load();

            GestionarSedeCentralController controlador = loader.getController();

            Scene scene = new Scene(root);
            Stage stage = new Stage();

            stage.setScene(scene);
            stage.show();

            stage.setOnCloseRequest(e -> controlador.closeWindows());

            Stage myStage = (Stage) this.btnManageSocios.getScene().getWindow();
            myStage.close();

        } catch (IOException ex) {
            Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onManageSedeDelegaciones(ActionEvent actionEvent) {
        if (this.existeSedeCentral()) {
            try {

                FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/GestionarDelegacionesVista.fxml"));
                Parent root = loader.load();

                GestionarDelegacionesController controlador = loader.getController();

                Scene scene = new Scene(root);
                Stage stage = new Stage();

                stage.setScene(scene);
                stage.show();

                stage.setOnCloseRequest(e -> controlador.closeWindows());

                Stage myStage = (Stage) this.btnManageSocios.getScene().getWindow();
                myStage.close();

            } catch (IOException ex) {
                Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("Debe crear una sede central antes de continuar.");
            alert.showAndWait();
        }
    }

    public void onManageSocios(ActionEvent actionEvent) {
        if (this.existeSedeCentral()) {
            try {

                FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/GestionarSociosVista.fxml"));
                Parent root = loader.load();

                GestionarSociosController controlador = loader.getController();

                Scene scene = new Scene(root);
                Stage stage = new Stage();

                stage.setScene(scene);
                stage.show();

                stage.setOnCloseRequest(e -> controlador.closeWindows());

                Stage myStage = (Stage) this.btnManageSocios.getScene().getWindow();
                myStage.close();

            } catch (IOException ex) {
                Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("Debe crear una sede central antes de continuar.");
            alert.showAndWait();
        }

    }

    private boolean existeSedeCentral() {
        try {
            CRUD<SedeCentral> crudSedeCentral = DAOFactoria.getFactoria(TipoEntidades.SedeCentral);

            SedeCentral sedeCentral = crudSedeCentral.leerPorId("0");
            return sedeCentral != null;

        } catch(HibernateException | NoExisteEntidadException err) {
            return false;
        }

    }
}
