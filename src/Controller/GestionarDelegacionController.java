package Controller;

import DAO.CRUD;
import Enums.TipoEntidades;
import Factorias.DAOFactoria;
import Model.Delegacion;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.hibernate.HibernateException;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GestionarDelegacionController {
    private Delegacion delegacionSeleccionada = null;
    public TextField tfieldDelegacionNombre;
    public TextField tfieldDelegacionDireccion;
    public Button btnGuardarDelegacion;

    public void initAttributtes(Delegacion delegacion) {
        if (delegacion != null) {
            this.delegacionSeleccionada = delegacion;

            this.tfieldDelegacionNombre.setText(this.delegacionSeleccionada.getNombre());
            this.tfieldDelegacionDireccion.setText(this.delegacionSeleccionada.getDireccion());
        }
    }

    public void closeWindows() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/GestionarDelegacionesVista.fxml"));

            Parent root = loader.load();

            GestionarDelegacionesController controlador = loader.getController();

            Scene scene = new Scene(root);
            Stage stage = new Stage();

            stage.setScene(scene);
            stage.show();

            stage.setOnCloseRequest(e -> controlador.closeWindows());

            Stage myStage = (Stage) this.btnGuardarDelegacion.getScene().getWindow();
            myStage.close();

        } catch (IOException ex) {
            Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onGuardarDelegacion(ActionEvent actionEvent) {
        String errores = this.getFormularioErrores();
        if (errores.length() != 0) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText(errores);
            alert.showAndWait();
        } else {
            if (this.delegacionSeleccionada == null) {
                this.crearDelegacion();
            } else {
                this.modificarDelegacion();
            }
            this.closeWindows();
        }
    }

    private void crearDelegacion() {
        String nombre = this.tfieldDelegacionNombre.getText();
        String direccion = this.tfieldDelegacionDireccion.getText();
        Delegacion delegacionACrear = new Delegacion(nombre, direccion);

        CRUD<Delegacion> crudDelegacion = DAOFactoria.getFactoria(TipoEntidades.Delegacion);

        try {
            crudDelegacion.registrar(delegacionACrear);

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setTitle("Información");
            alert.setContentText("Se ha creado la delegación correctamente.");
            alert.showAndWait();

        } catch(HibernateException err) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("Ha ocurrido un error al crear la delegación.");
            alert.showAndWait();
        }

    }

    private void modificarDelegacion() {
        String nombre = this.tfieldDelegacionNombre.getText();
        String direccion = this.tfieldDelegacionDireccion.getText();

        this.delegacionSeleccionada.setNombre(nombre);
        this.delegacionSeleccionada.setDireccion(direccion);

        CRUD<Delegacion> crudDelegacion = DAOFactoria.getFactoria(TipoEntidades.Delegacion);

        try {
            crudDelegacion.modificar(this.delegacionSeleccionada);

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setTitle("Información");
            alert.setContentText("Se ha modificado la delegación correctamente.");
            alert.showAndWait();

        } catch(HibernateException err) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("Ha ocurrido un error al modificar la delegación.");
            alert.showAndWait();
        }
        this.delegacionSeleccionada = null;

    }

    private String getFormularioErrores() {
        String errores = "";

        String nombre = this.tfieldDelegacionNombre.getText();
        String direccion = this.tfieldDelegacionDireccion.getText();

        if (nombre == null || nombre.trim().length() == 0) {
            errores += "- El campo nombre es invalido\n";
        }

        if (direccion == null || direccion.trim().length() == 0) {
            errores += "- El campo dirección es invalido\n";
        }
        return errores;
    }
}
