package DAO;

import Exceptions.NoExisteEntidadException;
import Model.Delegacion;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.ArrayList;
import java.util.List;

public class DelegacionDAOSQLImpl implements DelegacionDAO {


    @Override
    public List<Delegacion> listarTodos()  throws HibernateException {
        SessionFactory factoria = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Delegacion.class).buildSessionFactory();
        Session sesion = factoria.openSession();

        List<Delegacion> listaDelegaciones = new ArrayList<Delegacion>();

        try {
            sesion.beginTransaction();
            listaDelegaciones = sesion.createQuery("from Delegacion").getResultList();
            sesion.getTransaction().commit();

        } catch (HibernateException err) {
            sesion.getTransaction().rollback();
            throw err;
        } finally {
            if(sesion != null) {
                sesion.close();
            }
            factoria.close();
        }
        return listaDelegaciones;
    }

    @Override
    public Delegacion leerPorId(String id)  throws HibernateException, NoExisteEntidadException {
        SessionFactory factoria = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Delegacion.class).buildSessionFactory();
        Session sesion = factoria.openSession();

        List<Delegacion> listaDelegaciones = new ArrayList<Delegacion>();

        try {
            sesion.beginTransaction();
            listaDelegaciones = sesion.createQuery("from Delegacion d where d.nombre = :nombre").setParameter("nombre", id).getResultList();
            sesion.getTransaction().commit();

        } catch (HibernateException err) {
            sesion.getTransaction().rollback();
            throw err;
        } finally {
            if(sesion != null) {
                sesion.close();
            }
            factoria.close();
        }
        if (listaDelegaciones.size() == 1) {
            return listaDelegaciones.get(0);
        } else {
            throw new NoExisteEntidadException("ERROR - Entidad no encontrada");
        }
    }

    @Override
    public void registrar(Delegacion delegacion)  throws HibernateException {

        SessionFactory factoria = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Delegacion.class).buildSessionFactory();
        Session sesion = factoria.openSession();

        try {
            sesion.beginTransaction();
            sesion.save(delegacion);
            sesion.getTransaction().commit();

        } catch (HibernateException err) {
            sesion.getTransaction().rollback();
            throw err;
        } finally {
            if(sesion != null) {
                sesion.close();
            }
            factoria.close();
        }
    }

    @Override
    public void modificar(Delegacion delegacion)  throws HibernateException {
        SessionFactory factoria = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Delegacion.class).buildSessionFactory();
        Session sesion = factoria.openSession();

        try {
            sesion.beginTransaction();

            Delegacion delegacionAModificar= sesion.get(Delegacion.class, delegacion.getCodigo());

            delegacionAModificar.setNombre(delegacion.getNombre());
            delegacionAModificar.setDireccion(delegacion.getDireccion());

            sesion.getTransaction().commit();

        } catch (HibernateException err) {
            sesion.getTransaction().rollback();
            throw err;
        } finally {
            if(sesion != null) {
                sesion.close();
            }
            factoria.close();
        }
    }

    @Override
    public void eliminar(Delegacion delegacion)  throws HibernateException {
        SessionFactory factoria = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Delegacion.class).buildSessionFactory();
        Session sesion = factoria.openSession();

        try {
            sesion.beginTransaction();

            sesion.createQuery("delete Delegacion where codigo = :codigo").setParameter("codigo", delegacion.getCodigo()).executeUpdate();

            sesion.getTransaction().commit();

        } catch (HibernateException err) {
            sesion.getTransaction().rollback();
            throw err;
        } finally {
            if(sesion != null) {
                sesion.close();
            }
            factoria.close();
        }
    }

}
