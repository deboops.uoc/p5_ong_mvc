package DAO;

import Exceptions.NoExisteEntidadException;
import Model.SedeCentral;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.ArrayList;
import java.util.List;

public class SedeCentralDAOSQLImpl implements SedeCentralDAO {

    @Override
    public List<SedeCentral> listarTodos() throws HibernateException {
        SessionFactory factoria = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(SedeCentral.class).buildSessionFactory();
        Session sesion = factoria.openSession();

        List<SedeCentral> listaSedeCentral = new ArrayList<SedeCentral>();

        try {
            sesion.beginTransaction();
            listaSedeCentral = sesion.createQuery("from SedeCentral").getResultList();
            sesion.getTransaction().commit();

        } catch (HibernateException err) {
            sesion.getTransaction().rollback();
            throw err;
        } finally {
            if(sesion != null) {
                sesion.close();
            }
            factoria.close();
        }
        return listaSedeCentral;
    }

    @Override
    public SedeCentral leerPorId(String id) throws NoExisteEntidadException, HibernateException {
        SessionFactory factoria = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(SedeCentral.class).buildSessionFactory();
        Session sesion = factoria.openSession();

        List<SedeCentral> listaSedeCentral = new ArrayList<SedeCentral>();

        try {
            sesion.beginTransaction();
            listaSedeCentral = sesion.createQuery("from SedeCentral sd where sd.codigo = 1").getResultList();
            sesion.getTransaction().commit();

        } catch (HibernateException err) {
            sesion.getTransaction().rollback();
            throw err;
        } finally {
            if(sesion != null) {
                sesion.close();
            }
            factoria.close();
        }
        if (listaSedeCentral.size() == 1) {
            return listaSedeCentral.get(0);
        } else {
            throw new NoExisteEntidadException("ERROR - Entidad no encontrada");
        }

    }

    @Override
    public void modificar(SedeCentral s) throws HibernateException {
        // No se implementa, caso de uso no permitido
    }

    public void registrar(SedeCentral sede) throws HibernateException {

        SessionFactory factoria = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(SedeCentral.class).buildSessionFactory();
        Session sesion = factoria.openSession();

        try {
            sesion.beginTransaction();
            sesion.save(sede);
            sesion.getTransaction().commit();

        } catch (HibernateException err) {
            sesion.getTransaction().rollback();
            throw err;
        } finally {
            if(sesion != null) {
                sesion.close();
            }
            factoria.close();
        }

    }


    @Override
    public void eliminar(SedeCentral s) throws HibernateException {
        // No se implementa, caso de uso no permitido
    }
}