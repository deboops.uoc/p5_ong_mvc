package DAO;

import Exceptions.NoExisteEntidadException;
import Model.Socio;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.ArrayList;
import java.util.List;

public class SocioDAOSQLImpl implements SocioDAO {

    @Override
    public List<Socio> listarTodos() throws HibernateException {
        SessionFactory factoria = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Socio.class).buildSessionFactory();
        Session sesion = factoria.openSession();

        List<Socio> listaSocios = new ArrayList<Socio>();

        try {
            sesion.beginTransaction();
            listaSocios = sesion.createQuery("from Socio").getResultList();
            sesion.getTransaction().commit();

        } catch (HibernateException err) {
            sesion.getTransaction().rollback();
            throw err;
        } finally {
            if(sesion != null) {
                sesion.close();
            }
            factoria.close();
        }
        return listaSocios;
    }

    @Override
    public Socio leerPorId(String id) throws HibernateException, NoExisteEntidadException {
        SessionFactory factoria = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Socio.class).buildSessionFactory();
        Session sesion = factoria.openSession();

        List<Socio> listaSocios = new ArrayList<Socio>();

        try {
            sesion.beginTransaction();
            listaSocios = sesion.createQuery("from Socio s where s.identificador = :identificador").setParameter("identificador", id).getResultList();
            sesion.getTransaction().commit();

        } catch (HibernateException err) {
            sesion.getTransaction().rollback();
            throw err;
        } finally {
            if(sesion != null) {
                sesion.close();
            }
            factoria.close();
        }
        if (listaSocios.size() == 1) {
            return listaSocios.get(0);
        } else {
            throw new NoExisteEntidadException("ERROR - Entidad no encontrada");
        }
    }

    @Override
    public void registrar(Socio socio) throws HibernateException {
        SessionFactory factoria = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Socio.class).buildSessionFactory();
        Session sesion = factoria.openSession();

        try {
            sesion.beginTransaction();
            sesion.save(socio);
            sesion.getTransaction().commit();

        } catch (HibernateException err) {
            sesion.getTransaction().rollback();
            throw err;
        } finally {
            if(sesion != null) {
                sesion.close();
            }
            factoria.close();
        }
    }

    @Override
    public void modificar(Socio socio) throws HibernateException {
        SessionFactory factoria = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Socio.class).buildSessionFactory();
        Session sesion = factoria.openSession();

        try {
            sesion.beginTransaction();

            Socio socioAModificar= sesion.get(Socio.class, socio.getCodigo());

            socioAModificar.setIdentificador(socio.getIdentificador());
            socioAModificar.setNombre(socio.getNombre());
            socioAModificar.setFechaAlta(socio.getFechaAlta());
            socioAModificar.setFechaBaja(socio.getFechaBaja());

            sesion.getTransaction().commit();

        } catch (HibernateException err) {
            sesion.getTransaction().rollback();
            throw err;
        } finally {
            if(sesion != null) {
                sesion.close();
            }
            factoria.close();
        }
    }

    @Override
    public void eliminar(Socio socio) throws HibernateException {
        SessionFactory factoria = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Socio.class).buildSessionFactory();
        Session sesion = factoria.openSession();

        try {
            sesion.beginTransaction();

            sesion.createQuery("delete Socio where codigo = :codigo").setParameter("codigo", socio.getCodigo()).executeUpdate();

            sesion.getTransaction().commit();

        } catch (HibernateException err) {
            sesion.getTransaction().rollback();
            throw err;
        } finally {
            if(sesion != null) {
                sesion.close();
            }
            factoria.close();
        }
    }
}
