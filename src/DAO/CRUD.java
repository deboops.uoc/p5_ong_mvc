package DAO;

import Exceptions.NoExisteEntidadException;
import org.hibernate.HibernateException;

import java.util.List;

public interface CRUD<T> {

    List<T> listarTodos() throws HibernateException;
    T leerPorId(String id) throws HibernateException, NoExisteEntidadException;
    void registrar(T t) throws HibernateException;
    void modificar(T t) throws HibernateException;
    void eliminar(T t) throws HibernateException;
}
