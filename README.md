**Proyecto correspondiente a P5 - Programación orientada a objetos con acceso a base de datos.**

**Producto 4**

**Descripción**

Los pasos a seguir para llevar a cabo el producto son:
1. Leer detenidamente estas instrucciones e identificar los requerimientos de la
actividad.
2. Revisar detenidamente la rúbrica de evaluación.
3. Consultar los recursos necesarios facilitados en el aula.
4. Realizar una comparativa entre los diversas interfaces de programación de
persistencia de la aplicación con mapeo ORM, por ejemplo, JPA, Hibernate,
JDO, y escoger el más idóneo para aplicar en este proyecto.
5. Implementar de la aplicación de escritorio ONG Entreculturas utilizando el
patrón de diseño MVC utilizando JavaFX.

● La capa de la lógica de negocio estará formada por las clases
implementadas.

● La programación de la persistencia de la aplicación deberá realizarse
con la Interfaz de programación de aplicaciones (API) de mapeo ORM
seleccionada en el primer apartado.
